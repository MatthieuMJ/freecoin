# Free Coin Web App using React, Web3 and Metamask

This web application can communicate with Free Coin smart contract. It's split in two part :

1. Create/Open Free Coin smart contract with your metamask account .
     
2. Get balance or Transfer Free Coin with your account.

## How to run Free Coin Web App

Clone the repository in local :
    
`git clone git@bitbucket.org:MatthieuMJ/freecoin.git`

Install node module :
    
`npm i`

Start the application :
    
`npm start`

